---
layout: page
title:  "Apresentações e links"
permalink: /apresentacoes2020-2/
---

Aqui estão alguns materiais para download e links:
- [Apresentação de Boas vindas]({{ "/docs/2020-2/ApresentaçãoBemVindos.pdf" | absolute_url }})
- [Guia de referências de serviços para o período de pandemia]({{ "/docs/2020-2/GuiaPandemia.pdf" | absolute_url}})
- [Sistemas de Informação]({{ "/docs/2020-2/Sistemas-de-info.pdf" | absolute_url }})
- [Wiki da Rede do IME](https://wiki.ime.usp.br/)

Apresentações das CCPs:
- [CCP - MAT]({{ "/docs/2020-2/slide-CCP-MAT-2020-2.pdf" | absolute_url }})
- [CCP - MAP]({{ "/docs/2020-2/slide-CCP-MAP2020.pdf" | absolute_url }})

