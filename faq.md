---
layout: page
title: FAQ
permalink: /faq/
---

## O que são a carteirinha da USP e o BUSP?

A carteirinha da USP é necessária retirar livros da biblioteca e ter acesso a alguns lugares do campus. Existem duas linhas de ônibus (8012-10 e 8022-10) que percorrem o campus e vão até o metrô Butantã. Com o BUSP você pode usar o circular de graça.

## Como solicitar a carteirinha USP e o BUSP?

1. Abra o e-mail de aceite no programa de pós graduação do IME.
2. Pegue sua senha no Serviços de Informática (SI).
3. Acesse seu e-mail @ime.usp.br. No seu e-mail tem uma mensagem explicando com acessar o Janus.
4. Acesse o Janus.
5. Clique em cartões USP e clique em **Solicitar Bilhete USP** para solicitar o BUSP
6. Clique em **Nova solicitação** para pedir a cateirinha.
7. Após receber a carteirinha você precisa acessar o essa mesma página no Janus e ativar cartão.

## Onde fica o bandejão?

Há quatro restaurantes universitários no campus (apelidados de bandejão):

1. Restaurante Central (próximo à praça do relógio - CRUSP)
2. Restaurante da Física (no IF-USP)
3. Restaurante da Química (no IQ-USP)
4. Restaurante da Prefeitura (no PUSP-C)

O bandejão mais próximo do IME é o restaurante da física.

## Como solicitar/renovar o bilhete único?

Os alunos de pós-graduação têm direito a um desconto de 50% para o uso de transporte público pela SPTrans ou EMTU (EMTU apenas para quem não mora em São Paulo) através do passe escolar. O passe escolar da SPTrans (bilhete único escolar) permite o uso de ônibus em São Paulo e o uso de trens e metrô na região metropolitana.

Ao contrário do que acontece para alunos de graduação, a cota do bilhete único (quantidade de passagens que um aluno consegue comprar com desconto durante um mês) é calculada de acordo com a quantidade de dias da semana em que você vai assistir aula no semestre em que você solicitou o bilhete único. 

Para solicitar o bilhete único você precisa comprovar a quantidade de dias que você frequenta a USP. Existem duas formas de fazer isso.

1) Você pode imprimir o comprovante de matrícula no Janus com a lista de disciplinas.
2) Outra possibilidade é preencher o formulário de declaração de frequência, indicando quais dias da semana você vai para a USP estudar. Você precisa pedir o formulário na secretaria. Nesse caso você precisa da assinatura do orientador.  Você também precisa imprimir o comprovante de matrícula simples no Janus.

Nos dois casos é necessário entregar o documento no SAS

Após a USP realizar o cadastro, você precisa se cadastrar no site da SPTrans e pagar o boleto. No início de cada ano é necessário repetir esse processo para renovar o bilhete.

## O IME possui cursos de verão?

Sim, o instituto oferece cursos de verão. Muitos deles são voltados para alunos de graduação e para o público geral. São esses listados [aqui](www.ime.usp.br/~verao). Existem também cursos de verão voltados para alunos de pós. Veja as disciplinas [aqui](https://www.ime.usp.br/pos/ferias). Você pode aproveitar os créditos dos cursos, mas apenas dos cursos voltados para a pós **conferir**

Você não precisa ter começado a pós para fazer os cursos de verão da pós. Confira o [site](https://www.ime.usp.br/pos/ferias) para mais informações

