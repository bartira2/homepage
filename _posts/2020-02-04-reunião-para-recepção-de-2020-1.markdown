---
layout: post
title:  "Reunião para Recepção de 2020.1"
date:   2020-02-04 14:00:00 -0300
categories: jekyll update
---

Olás! Um bom 2020 a todxs!

A Comissão de Recepção de Pós-Graduação está preparando a recepção do primeiro semestre de 2020 e nós gostaríamos de convidar a todxs que queiram contribuir para a organização do evento!

Faremos nossa próxima reunião esta quinta-feira, 4/2, das 14h às 16h, na sala 243 do bloco A. Venha conhecer a comissão e nosso trabalho =)

No momento estamos caminhando para os preparativos finais como, por exemplo, a programação do dia da recepção.

[Evento no facebook](https://www.facebook.com/events/173911763883370/)
