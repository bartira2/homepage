---
layout: post
title:  "Recepção do 2º semestre de 2019"
date:   2019-07-11 19:30:00 -0300
categories: jekyll update
---

Olá Pessoal !!!

A próxima Recepção da Pós será no dia 06-08-19 (terça) a partir das 13h30 na sala 5 do bloco B.

[Evento no Facebook](https://www.facebook.com/events/2511711068914786/)

Slides apresentados nas CCPs:

+ [Aplicada]({{ "/docs/2019-2/apresentacao-ccp-aplicada.pdf" | absolute_url }})
