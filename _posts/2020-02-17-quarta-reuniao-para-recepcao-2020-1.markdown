---
layout: post
title:  "Reunião para Recepção de 2020.1"
date:   2020-02-17 14:40:00 -0300
categories: jekyll update
---

Olás!

A Comissão de Recepção de Pós-Graduação está preparando a recepção do primeiro semestre de 2020 e nós gostaríamos de convidar a todxs que queiram contribuir para a organização do evento!

Faremos nossa próxima reunião esta quinta-feira, 20/02, das 14h30min às 16h30min, no IME, na sala de reuniões do bloco C. Venha conhecer a comissão e nosso trabalho =)

No momento estamos caminhando para os preparativos finais!
