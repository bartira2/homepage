---
layout: post
title:  "Terceira reunião para a recepção de 2020-1"
date:   2019-10-11 15:17:41 -0300
categories: jekyll update
---

Olá,

Na próxima quinta, dia *17/10*, teremos uma reunião para organizar a recepção do início de 2020. Entre outras coisas, vamos falar sobre um documento que vamos fazer para divulgar a recepção para a reitoria e também vamos pensar em ideias para melhorar as atividades de socialização no dia da recepção. Fique à vontade para dar ideias e fazer sugestões!

A reunião será na quinta, dia 17, às 18h na sala B16.

Até mais