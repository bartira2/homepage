---
layout: post
title:  "Vamos bandejar? (primeiro semestre de 2019)"
date:   2019-02-23 09:23:00 -0300
categories: jekyll update
---

Olá,

Para vocês conhecerem melhor seus colegas, pretendemos ir ao bandejão almoçar juntos nesta quinta-feira (28/2). Vamos nos encontrar às 11:30 no saguão do bloco B, para resolver os detalhes do cartão. Sairemos pro bandejão às 12:00.

Para conseguir comer no bandejão, é necessário que você já tenha feito matrícula na CPG (secretaria de pós, sala 27 do bloco B). Instale o aplicativo e-Card, assim você tem acesso ao bandejão enquanto a sua carteirinha não chega. Mais informações: [http://www.prg.usp.br/?p=27481]({{ "http://www.prg.usp.br/?p=27481" | absolute_url }})

Siga os passos abaixo para solicitar a sua carteirinha USP e também o BUSP, para poder usar o circular sem pagar.

![Solicitando a carteirinha USP e o BUP]({{ "imgs/esquema-solicitacao-carteiria-usp-busp.jpg" | relative_url }})

Até mais!