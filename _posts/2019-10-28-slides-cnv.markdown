---
layout: post
title:  "Slides sobre Comunicação Não-Violenta"
date:   2019-10-28 15:00:00 -0300
categories: jekyll update
---

No dia 17/10/19 no Café, Giz e Demonstração, foi realizado o seminário "Princípios da Comunicação Não-Violenta". Para quem tem interesse no material utilizado e as referências mencionadas, [estamos disponibilizando o
material]({{ "/docs/cgd/Estudo de CNV 2019v1.1.pdf" | absolute_url }}).

Sobre o projeto Café, Giz e Demonstração: são seminários realizados por alunos(as) para alunos(as) com linguagem acessível e ao final há café e bolachas. Caso tenha interesse em apresentar um seminário, entre em contato com analuiza at ime.usp.br. Siga o projeto pelo instagram: @cafegizdem

Importante: Esse material foi elaborado pela Comissão de Recepção da Pós-Graduação do Instituto de Matemática e Estatística da USP (IME-USP), sob a licença Creative Commons Atribuição-CompartilhaIgual 4.0 Internacional (CC BY-SA 4.0).

