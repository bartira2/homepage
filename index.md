---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
list_title: "Notícias"
---
![Foto da recepção]({{ "/imgs/recepcao2018-1.jpeg" | relative_url }})


# Recepção de Pós-Graduação


Olá! Essa página é dedicada à Recepção da Pós-Graduação do IME-USP.

Aqui divulgamos as informações relacionadas a Recepção de Pós Graduação (RPG), assim como alguns materiais que distribuímos no dia da recepção.

A recepção para ingressantes do segundo semestre de 2020 ocorrerá virtualmente no dia 11 de Agosto de 2020 a partir das 13h30 em meet.google.com/wvz-kwsa-kwx.



