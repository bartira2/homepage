---
layout: page
title: Localização do Instituto
permalink: /localização/
wip: true
---

O Instituto de Matemática e Estatística da Universidade de São Paulo (IME-USP)
fica localizado na rua do Matão, número 1010, Cidade Universitária, CEP:
05508-090, São Paulo/SP.

Confira aqui o [Mapa do
Campus]({{ "https://www.ime.usp.br/images/arquivos/imagens/cidadeuniversitaria.pdf" | absolute_url }})

![Foto do IME]({{ "/imgs/ime.jpg" | absolute_url }})

## Utilizando transporte público

### METRÔ

A estação mais próxima da USP é a Estação Butantã, na linha 4 - Amarela. 

[Confira aqui o mapa da rede do Metrô de São Paulo]({{ "http://www.metro.sp.gov.br/pdf/mapa-da-rede-metro.pdf?404%3bhttp%3a%2f%2fwww.metro.sp.gov.br%3a80%2fredes%2fteredes.shtml" | absolute_url }}).

### CIRCULAR – USP

Os ônibus circulares operados pela USP são linhas internas e gratuitas que complementam o transporte feito pelas linhas 8012, 8022 e 8032 (gratuito aos portadores do Bilhete USP), 

[Confira aqui o trajeto Linhas 8012, 8022 e 8032]({{ "http://www.puspc.usp.br/wp-content/uploads/sites/159/2016/08/Circular-3-Nova-Linha.pdf" | absolute_url }})

![Foto do Circular]({{ "/imgs/circular1.jpg" | absolute_url }})

![Foto do itinerário do Circular]({{ "/imgs/circular2.jpg" | absolute_url }})

Do aeroporto internacional de Guarulhos para a USP, [pode-se utilizar o seguinte trajeto]({{ "https://www.google.com/maps/dir/Aeroporto+Internacional+de+S%C3%A3o+Paulo,+Rod.+H%C3%A9lio+Smidt,+s%2Fn%C2%BA+-+Aeroporto,+Guarulhos+-+SP,+07190-100/Universidade+de+S%C3%A3o+Paulo+-+Butanta,+S%C3%A3o+Paulo+-+SP/@-23.4982525,-46.6736301,12z/data=!3m1!4b1!4m14!4m13!1m5!1m1!1s0x94ce8af96f722a25:0x8071626c51a7154a!2m2!1d-46.473043!2d-23.4305731!1m5!1m1!1s0x94ce5611e0f05911:0x4f4f4d2c7e06e56!2m2!1d-46.7307891!2d-23.5613991!3e3" | absolute_url }}) (de transporte público).

## Alojamento especial

Para alunos e alunas da pós-graduação, [a USP oferece alojamentos emergenciais]({{ "https://sites.usp.br/sas/wp-content/uploads/sites/265/2019/02/2019-Site-SAS-orienta%c3%a7%c3%b5es-alojamento-PG.pdf" |  absolute_url }}).

## Instagram

Para conhecer melhor nosso trabalho, [consulte o Instagram da Comissão de Recepção da Pós-Graduação]({{ "https://www.instagram.com/comissaorpg/" | absolute_url }})

